FROM registry.access.redhat.com/ubi8/php-74

# List of packages to install.

ARG EXTRA_PACKAGES="oracle-instantclient-basic \
  oracle-instantclient-devel \
  libaio \
  php-devel \
  mailx \
  https://linuxsoft.cern.ch/mirror/yum.oracle.com/repo/OracleLinux/OL8/developer/x86_64/getPackage/php-oci8-21c-2.2.0-1.0.1.module+el8.5.0+20481+e337f400.x86_64.rpm \
  https://linuxsoft.cern.ch/internal/repos/dbclients8-stable/x86_64/os/Packages/oracle-instantclient-tnsnames.ora-1.4.5-1.el8.cern.noarch.rpm"

# The following installs most CERN Centos Stream 8 repos including Oracle drivers and EPEL. More repos can be added if needed.

ARG EXTRA_REPOS="https://linuxsoft.cern.ch/cern/rhel/8/appstream/x86_64/os/Packages/m/mod_auth_openidc-2.4.9.4-1.module+el8.7.0+14797+4085fcb6.x86_64.rpm \
http://linuxsoft.cern.ch/cern/rhel/8/appstream/x86_64/os/Packages/c/cjose-0.6.1-2.module+el8+2454+f890a43a.x86_64.rpm \
https://linuxsoft.cern.ch/cern/rhel/8/CERN/x86_64/Packages/o/oracle-release-1.5-2.rh8.cern.noarch.rpm"
# Temporarily switch to root user to install packages

USER root

RUN rpm --import https://linuxsoft.cern.ch/mirror/yum.oracle.com/RPM-GPG-KEY-oracle-ol8 \
&& dnf install -y ${EXTRA_REPOS} \
&& dnf install -y ${EXTRA_PACKAGES} \
&& dnf clean all

ENV TNS_ADMIN=/usr/lib/oracle/21/client64

RUN wget https://github.com/php/php-src/archive/refs/tags/php-7.4.33.tar.gz

RUN ln -s /usr/include/oracle/21/client64/* /usr/include/.

RUN gunzip php-7.4.33.tar.gz && tar -xvf php-7.4.33.tar && cd php-src-php-7.4.33/ext/pdo_oci && phpize && ./configure --with-pdo-oci=/usr/lib/oracle/21/client64/ && make install

RUN echo "extension=pdo_oci.so" >> /etc/php.d/20-pdo.ini

# Run the final image as unprivileged user.
# This is the base image's `default` user, but S2I requires a numerical user ID.

USER 1001